# Sunspot prediction
# input: RADAILY.PLT from web
# output: MONTLY monthly average from daily RADAILY.PLT data
#         SMOOTHED smoothed monthly averages from MONTHLY

# Note: monthly and smoothed data were collected in lists before writing to a file.
#       It would have been easier to simply write the data directly to a file.
#       However, creating lists made testing easier.


# central formula for smoothing around index i: x is the new value
# have to start with i = 6 and end with i + 5 = last index = len(yL)-1
# Since I was creating a list I had to ensure that I wasn't smoothing smoothed data.
def smoothData(yearList):
    yL = [i[:] for i in yearList]
    for i in range(6,len(yL)-6):
        yL[i][2] = (yearList[i-6][2]/2.0 + sum([n[2] for n in yearList[i-6+1:i+6]]) + yearList[i+6][2]/2.0)/12.0
    return(yL)

# initial values for loop
year = '1945'
month = '01'
yearList = [] # list of all years monthly averages

# initialize sum and count
monthSum = 0   # running sunspot sum
monthCount = 0 # count of days in a month

# read line-by-line from file
for line in file("RADAILY.PLT"):
    if len(line.split()) == 4: #eliminates days with partial records, e.g. 2007
        # get year and month from current line
        thisYear = line.split()[0]
        thisMonth = line.split()[1]
            
        if month != thisMonth: # new month
            # record current month's data and set up for new month
            # check for divide by zero
            if monthCount != 0:  
                yearList.append([year, month, float(monthSum)/monthCount])
            # set up for next month
            month = thisMonth
            year = thisYear
            monthSum = 0
            monthCount = 0

        # add to running sum and increment count of days
        monthSum += int(line.split()[3])
        monthCount += 1
        
else:  #handle last month of last year
    if monthCount != 0:
        yearList.append([year,month,float(monthSum)/monthCount])

yL = smoothData(yearList)

# write data out to files        
foutMonthly = open("MONTHLY", "w")
foutSmoothed = open("SMOOTHED", "w")

for data in yearList:
    foutMonthly.write("%-6s %3s %6.1f \n"%(data[0],data[1],data[2]))

for data in yL:
    foutSmoothed.write("%-6s %3s %6.1f \n"%(data[0],data[1],data[2]))


foutMonthly.close()
foutSmoothed.close()

    

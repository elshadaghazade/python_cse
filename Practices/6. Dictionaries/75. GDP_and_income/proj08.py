# uncomment for testing with run_file.py
#import sys
#def input( prompt=None ):
#    if prompt != None:
#        print( prompt, end="" )
#    aaa_str = sys.stdin.readline()
#    aaa_str = aaa_str.rstrip( "\n" )
#    print( aaa_str )
#    return aaa_str
    
import pylab

# Here are some constants that are optional to use -- feel free to modify them, if you wish
REGION_LIST = ['Far_West',
 'Great_Lakes',
 'Mideast',
 'New_England',
 'Plains',
 'Rocky_Mountain',
 'Southeast',
 'Southwest',
 'all']
VALUES_LIST = ['Pop', 'GDP', 'PI', 'Sub', 'CE', 'TPI', 'GDPp', 'PIp']
VALUES_NAMES = ['Population(m)','GDP(b)','Income(b)','Subsidies(m)','Compensation(b)','Taxes(b)','GDP per capita','Income per capita']
PROMPT1 = "Specify a region from this list -- far_west,great_lakes,mideast,new_england,plains,rocky_mountain,southeast,southwest,all: "
PROMPT2 = "Specify x and y values, space separated from Pop, GDP, PI, Sub, CE, TPI, GDPp, PIp: "

def plot_regression(x,y):
    '''Draws a regression line for values in lists x and y.
       x and y must be the same length.'''
    xarr = pylab.array(x) #numpy array
    yarr = pylab.array(y) #numpy arry
    m,b = pylab.polyfit(xarr,yarr, deg = 1) #creates line, only takes numpy arrays
    #as parameters
    pylab.plot(xarr,m*xarr + b, '-') #plotting the regression line
    


def plot(pass):   # you need to replace pass with parameters
    '''Plot the values in the parameters.'''
    pass # placeholder
    
    # prompt for which values to plot; these will be the x and y
    
    # build x, the list of x values
    # build y, the list of y values
    # hint: list comprehension is a slick way to build x and y

    # In the following you need to replace 'pass' with your own arguments
    # pylab.title(pass)   # plot title

    #pylab.xlabel(pass)   #label x axis
    #pylab.ylabel(pass)   #label y axis
    
    #pylab.scatter(x,y)
    #for i, txt in enumerate(state_names): 
        #pylab.annotate(txt, (x[i],y[i]))
    
    #plot_regression(x,y)
    
    # USE ONLY ONE OF THESE TWO
    #pylab.show()                # displays the plot      
    #pylab.savefig("plot.png")   # saves the plot to file plot.png


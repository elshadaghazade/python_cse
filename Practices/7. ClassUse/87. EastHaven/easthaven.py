
import platform
import os

versionMinor = platform.python_version_tuple()[1]
versionMinor = int(versionMinor)
if versionMinor == 5:
    if os.path.isfile('implementation25.pyc'):
        print "loading python 2.5 version"
        import implementation25
    else:
        print "You need the file 'implementation25.pyc' from the project directory"
elif versionMinor == 6:
    if os.path.isfile('implementation26.pyc'):
        print "loading python 2.6 version"
        import implementation26
    else:
        print "You need the file 'implementation26.pyc' from the project directory\n"
else:
    print "Contact punch@msu.edu for a version of the file for your python version: %s"%(platform.python_version())

import cards

def setup():
    '''
    paramaters: None
    returns a tuple of:    
    - a foundation (list of 4 empty lists)
    - a tableau (a list of 7 lists, each sublist contains 3 cards, and the first two cards of each list should be set to hidden)
    - a stock contains a list of 31 cards. (after dealing 21 cards to tableau)
    '''
    pass

def printGame(fdation,tableau,stock):
    '''
    parameters: a foundation, a tableau and a stock
    returns: Nothing
    prints the game, i.e, print all the info user can see.
    Includes:
        a) print tableau  (Make sure only show those cards which are revealed. For hidden cards, just print "XX".
        b) print foundation
        c) print stock  (only need to show how many cards left in stock)
    '''
    pass
    
    

def revealCard(tableau, tRow):
    '''
    parameters: a tableau row and a tableau
    returns: Nothing
    reveal the top card of the indicated row
    '''
    pass
    
    
          
def moveToFoundation(tableau,fdation,tRow,fRow):
    '''
    parameters: a tableau, a foundation, row of tableau, row of foundation
    returns: Boolean (True if the move is valid, False otherwise)
    moves a card at the end of a row of tableau to a row of foundation
    reveal the next card in tableau if it is not already revealed.
    '''
    pass
    

def canBeConnected(card1, card2):
    '''
    parameters: two cards
    return: Boolean
    if the second card has different color from the first one, and the rank of card2 is one less than that of card1, return True
    Otherwise, return False
    '''
    pass
    

def moveInTableau(tableau,NumOfCards,tRowSource,tRowDest):
    '''
    parameters: a tableau, number of cards, the source tableau row and the destination tableau row
    returns: Boolean
    moves a certain number of cards from one row to another
    hint: 1. first make sure the cards you are moving are built down by rank and by alternating color
          2. if the dest row is empty, move those cards
             else, make sure the card of tableau[tRowSource][-NumofCards] is one rank lower than the top card of destination row (tableau[tRowDest][-1]),
                     and they should have different color
    '''
    pass
    
        


def dealMoreCards(stock,tableau):
    '''
    parameters: a stock and a tableau
    returns: Boolean
    deal one card to each row of tableau. For the last deal operation, deal the remaining cards to the first couple rows of tableau.
    returns False if the stock is empty. Otherwise, deal cards, and return True
    '''
    pass
    

def isWinner(fdation):
    '''
    parameters: a fdation
    return: Boolean
    If the fdation contains all 52 cards, return True
    else return False
    '''
    pass



def printRules():
    '''
    parameters: none
    returns: nothing
    prints the rules
    '''
    pass

def showHelp():
    '''
    parameters: none
    returns: nothing
    prints the supported commands
    '''
    pass

def play():
    ''' 
    main program. Does error checking on the user input. 
    '''
    pass

play()


        
    


import cards

def setup():
    '''
    paramaters: None
    returns a tuple of:
    - a stock (a shuffled deck after dealing 35 cards to tableau and 1 card to foundation)
    - a foundation (with one card)
    - a tableau (list of 7 lists, each has 5 cards)
    '''


def printGame(stock,fdation,tableau):
    '''
    parameters: a stock, a foundation and a tableau
    returns: Nothing
    prints the current situation of the game
    '''


def canMove(tRow,tableau,fdation):
    '''
    parameters: a row num, a tableau and a tableau
    returns: Boolean
    tests if the card at the end of the row can be moved to the foundation
    '''

          
def moveToFoundation(tRow,tableau,fdation):
    '''
    parameters: a row num, a tableau and a foundation
    returns: nothing
    moves a card at the end of a row to the foundation if it can
    '''
 


def dealMoreCards(stock,fdation):
    '''
    parameters: a stock and a tableau
    returns: Boolean
    deal one card from stock to foundation
    returns False if it runs out of cards
    '''


def isWinner(tableau):
    '''
    parameters: a tableau
    return: a Boolean
    is this a winning position. No cards left in each tableau row.
    '''



def isLoser(stock, fdation, tableau):
    '''
    parameters: a stock, a foundation and a tableau
    return: a Boolean
    is this a Loser position.
    Loser position should satisfy two conditions:
    1. No cards left in stock.
    2. No possible move from tableau
    '''



def printRules():
    '''
    parameters: none
    returns: nothing
    prints the rules
    '''
    print "Rules of Golf Relaxed"
    print "Goal, move all the cards to the foundation"
    print "1) All cards move from the tableau (7 rows) to the foundation"
    print "2) Only the last card in each row of the tableau can be moved"
    print "3) You may move a card from the tableau to the foundation when there is a card"
    print "   in the tableau whose rank is adjacent to the rank of the top card of the foundation."
    print "   Ace and King are considered adjacent. "
    print "4) If there are remaining card(s) in the stock. You may deal one card at a time "
    print "   to the top of the foundation. "    
    print


def play():
    ''' 
    main program. Does error checking on the user input. 
    '''


play()

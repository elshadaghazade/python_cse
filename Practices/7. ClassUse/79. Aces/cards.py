import random

class CCard():
    "denote a card with rank and suit"
    def __init__(self, cr=0, cs=''):
        # rank is one less than face value, i.e. a four of hearts has rank value of 3
        # why? for indexing, of course: see the disp_card method
        self.__rank = cr #0-Ace 1-9 face value ... 10-Jack 11-Queen 12-King
        self.__suit = cs #CSDH club spade diamond heart or empty string for blank (no card)

    def set_rank(self, cr): self.__rank = cr #set the rank of the card 0-12                
        
    def set_suit(self, cs): self.__suit = cs #set the suit CSDH                
        
    def get_rank(self): return self.__rank #returns 0-12        
        
    def get_suit(self): return self.__suit #return char one of CSDH
        
    #get the value on the face card 2-10 (Jack, Queen, King = 10) Ace = 1
    def get_val(self): 
        if self.__suit == "": return 0            
        if self.__rank < 10: return self.__rank + 1 #if Ace not high, the use "<"
        return 10 #return Jack, Queen or King
        
    #display a card to the string
    def display(self, string): return string + self.disp_card()
        
    def disp_card(self): #return the card display as a string
        if self.__suit == "": return "blk"
        return (" A 2 3 4 5 6 7 8 9 10 J Q K".split()[self.__rank] + self.__suit).rjust(3)

class CDeck():
    "denote a deck which to play cards on"
    #make a deck with all 52 cards
    def __init__(self): self.__deck = [CCard(i, "CSDH"[j]) for j in range(0,4) for i in range(0,13)]
        
    def shuffle(self): #shuffle however many cards are in the deck
        for i in range(0, random.randint(1,10)): random.shuffle(self.__deck)

    #return a card off the top and remove
    def deal(self): return ((len(self.__deck)>0) and [self.__deck.pop(-1)] or [CCard()])[0]
  
    def discard(self, n): #remove n cards from the top of the deck
        for i in range(0,n):
            if len(self.__deck) >= 0: del self.__deck[-1]

    def top(self): return ((len(self.__deck)>0) and [self.__deck[-1]] or [CCard()])[0]

    def bot(self): return ((len(self.__deck)>0) and [self.__deck[0]] or [CCard()])[0]

    def swaptop(self, c):
        if len(self.__deck) > 0:
            self.__deck.insert(-1, c)
            return self.__deck.pop(-1)
        else: return c

    def swapbot(self, c):
        if len(self.__deck) > 0:
            self.__deck.insert(0, c)
            return self.__deck.pop(1)
        else: return c            

    def pileon(self, c): self.__deck.append(c) #put a card on the deck

    def cardsleft(self): return len(self.__deck) #how many card left in the deck

    def play(self, n): return ((n<len(self.__deck)) and [self.__deck.pop(n)] or [CCard()])[0]

    def display(self): #very useful for debug shows the cards in the deck
        for i in range(0,len(self.__deck)):
            if i%13 == 0: print
            print self.__deck[i].disp_card(),

    def empty(self): return len(self.__deck) == 0

def displayGame(cardList):
    if len(cardList) < 21:
        print "Error in displayGame: cardList too short!"
        return
    print "Deal: ", cardList[0].disp_card()
    position = 1
    for i in range(16):
        if (i == 5) or (i == 10) or (i == 13):
            print
            if (i == 10) or (i == 13):
                print "%4s" % (" "),
        if cardList[i+1].get_val() == 0:
            print "%4d" % (position),
        else:
            print "%4s" % (cardList[i+1].disp_card()),
        position = position + 1
    print
    print
    print "Discards: ",
    for i in range(4):
        if (i == 2):
            print
            print "          ",
        if cardList[i+17].get_val() == 0:
            print "%4d" % (position),
        else:
            print "%4s" % (cardList[i+17].disp_card()),
        position = position + 1
    print

    

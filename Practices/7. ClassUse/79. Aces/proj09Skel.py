import cards

def setup():
    '''
    paramaters: None
    returns a tuple of:
    - a new shuffled Deck
    - a foundation (empty list)
    - a tableau (list of 4 empty lists)
    '''
    pass

def printGame(fdation,tableau):
    '''
    parameters: a foundation and a tableau
    returns: Nothing
    prints the game
    '''
    pass

def canMove(tRow,tableau):
    '''
    parameters: a tableau row and a tableau
    returns: Boolean
    tests if the card at the end of the row can be moved to the foundation
    '''
    pass
          
def moveToFoundation(tRow,fdation,tableau):
    '''
    parameters: a row num, a foundation and a tableau
    returns: nothing
    moves a card at the end of a row to the foundation if it can
    '''
    pass

def moveInTableau(tRowSource,tRowDest,tableau):
    '''
    parameters: the source, the dest tableau rows and a tableau
    returns: nothing
    moves the end of the source row to a hole if it can
    '''
    pass

def noHoles(tableau):
    '''
    parameters: a tableau
    returns: Boolean
    tests to see if there are empty rows to be filled
    '''
    pass

def dealMoreCards(deck,tableau):
    '''
    parameters: a deck and a tableau
    returns: Boolean
    adds more cards to the tableau if there are no holed to fill.
    returns False if it runs out of cards
    '''
    pass

def isWinner(deck,tableau):
    '''
    parameters: a deck and a tableau
    return: a Boolean
    is this a winning position. 1 ace in each tableau row, empty deck
    '''
    pass

def printRules():
    '''
    parameters: none
    returns: nothing
    prints the rules
    '''
    print "Rules of Aces Up"
    print "Goal, move all the cards to the foundations"
    print "1) All cards move from the tableau (4 rows) to the foundation"
    print "2) Only the last card in each row of the tableau can be moved"
    print "3) You may move a card from the tableau to the foundation when there is a card"
    print "   in the tableau of the same suit but higher rank"
    print "4) You may deal more cards, 1 to each row, when no moves are possible and there"
    print "   are no 'holes'"
    print "5) A hole is an empty column. It may be filled by any card"
    print


def play():
    ''' 
    main program. Does error checking on the user input. 
    '''
    pass


        
    


from cards import CCard, CDeck, displayGame

my_deck = CDeck()
my_card = CCard()
my_pile = CDeck()

#check if my_deck is empty(it isn't)
print "empty my_deck?", my_deck.empty()

#shuffle
my_deck.shuffle()
print "my_deck after shuffle"
my_deck.display()
print
print "-"*53

#deal a card from top of the deck named "my_deck"
my_card = my_deck.deal()
print "card dealt from top of my_deck:", my_card.disp_card()
print "-"*53
print "main deck after dealing one card to my_card"
my_deck.display()
print
print "-"*53

print "my_card", my_card.disp_card()
#get rank of card
print "rank of my_card", my_card.get_rank()

print "-"*53
#demo displayGame function
# first create a dummy set of cards to pass to the function
# so that only the positions will be printed
cardList = [CCard() for i in range(21)]
displayGame(cardList)

print "-"*53
# generate some cards to test the displayGame function
# Note that suits don't matter in this game.
# Also, when creating a card counting starts at zero, e.g. ace = 0, 1 = 2, etc.
card1 = CCard(7,'D')  # create a eight-of-diamonds
card2 = CCard(11,'H')  # create a queen-of-hearts
card3 = CCard(0,'S')   # create an ace-of-spades

#put the cards in some "random" slots and then display
cardList[2] = card1
cardList[12] = card2
cardList[8] = card3
displayGame(cardList)



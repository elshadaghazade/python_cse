Enter a text adventure filename: tworooms.txt

Welcome to this Generic Text Adventure!  You may explore and collect any items you find.  There's no winning, just exploration.

Valid commands are: 
L or look - print a description of the room you are in
I or inventory - print your inventory
P blah or pickup blah - pick up the item called blah
D blah or drop blah - drop the item called blah
N or north - go north
S or south - go south
E or east - go east
W or west - go west
Q or quit - quit
H or help - print this list of commands

Capitalization doesn't matter.

You see a normal room.  It has one exit, to the north.
Enter a command (H for help): look

You see a normal room.  It has one exit, to the north.
Enter a command (H for help): north

You move into the room to the north.
You see a treasure-filled room.  Scattered about are a sword, a shield, a diamond, and a ruby.  It has one exit, to the south.
Enter a command (H for help): south

You move into the room to the south.
You see a normal room.  It has one exit, to the north.
Enter a command (H for help): q

Goodbye

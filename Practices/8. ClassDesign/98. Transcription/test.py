import proj10

t = proj10.Transcriber("codonTable.txt")

# START is first codon, STOP is last codon, all aAcids are Arg
str1='ATGAGGAGACGGCGATAA'
print 'First is Start, last is Stop, rest are Arg:',t.transcribe(str1)

# never STARTS
str2='AAAAAAAA'
print 'Never Starts:',t.transcribe(str2)

# never STOPS
str3='ATGAGGAGACGGCGA'
print 'Never Stops:',t.transcribe(str3)

# junk before START and after STOP, all aAcids are Ala
str4 = 'AAAAAAATGGCTGCCGCAGCGTAGAAAAAAA'
print 'Good string, all Ala, junk before and after:',t.transcribe(str4)

# wrong letters in the string
str5 = 'ATGAGGAGACGGCGXTAA'
print 'Bad codon in string:',t.transcribe(str5)


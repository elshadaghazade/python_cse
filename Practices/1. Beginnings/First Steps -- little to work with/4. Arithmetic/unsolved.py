number1 = int(input("Please enter the first integer: "))
number2 = int(input("Please enter the second integer: "))

sum = ?
diff = ?
product = ?
quotient_integer = ?
quotient_remainder = ?

print("The sum of", number1, "and", number2, "is:", sum)
print("The difference of", number1, "and", number2, "is:", diff)
print("The product of", number1, "and", number2, "is:", product)
print("The quotient of", number1, "and", number2, "is:", quotient_integer, "with remainder", quotient_remainder)
# wct_index = 35.74 + 0.6215 * air_temp - 35.75 * air_speed**0.16 + 0.4275 * air_temp * air_speed**0.16

air_temp = 10.0
air_speed = 15
wct_index = ?

print("Air temperature:", air_temp, "degrees")
print("Wind speed:", air_speed, "MPH")
print("Wind chill index:", wct_index)

print("=====================================")

air_temp = 0.0
air_speed = 25
wct_index = ?

print("Air temperature:", air_temp, "degrees")
print("Wind speed:", air_speed, "MPH")
print("Wind chill index:", wct_index)

print("=====================================")

air_temp = -10.0
air_speed = 35
wct_index = ?

print("Air temperature:", air_temp, "degrees")
print("Wind speed:", air_speed, "MPH")
print("Wind chill index:", wct_index)

print("=====================================")

air_temp = float(input("Please enter the temperature (degrees F):"))
air_speed = float(input("Please enter the wind speed (MPH):"))
wct_index = ?

print("Air temperature:", air_temp, "degrees")
print("Wind speed:", air_speed, "MPH")
print("Wind chill index:", wct_index)

print("=====================================")



# This program generates an encrypted message for the project
# It has fixed input and output as well as fixed shift
# It uses a shift-10 Caesar cipher
# It keeps spaces unencrypted and throws out all other non-letters

letters = []
for c in range(26):
    letters.append(chr((c + 10)%26 + ord('a')))

words = ""
for line in file("source"):
    words = words + line + " "

encrypted = ""
for c in words:
    c = c.lower()
    if c == ' ':
        encrypted += c
    elif ord('a') <= ord(c) <= ord('z'):
        encrypted += letters[ord(c)-ord('a')]
    #else do nothing with other characters

f = open('sourceEncrypt','w')
f.write(encrypted)

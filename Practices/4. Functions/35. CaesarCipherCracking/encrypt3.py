from os import *

# Read in cipher text.  Add space at end of line.
words = ""
for line in file("cipherText"):
    words = words + line + " "

# Count characters
charCounts = [0]*26 # initialize 26 letter counters
for c in words:
    c = c.lower()
    if 'a' <= c <= 'z':
        charCounts[ord(c)-ord('a')] += 1
    # else do nothing with other characters

# Find biggest 
maxCharCount = max(charCounts)
maxCharIndex = charCounts.index(maxCharCount) #index into charCounts corresponds to ord(letter)

# Find shift: maxCharIndex - 'e'Index
shift = maxCharIndex - (ord('e')-ord('a')) # e is most common letter

# Using shift make a decryption key
key = []
for c in range(26):
    key.append(chr((c + shift)%26 + ord('a'))) # find index then convert to character

# Apply decryption key to cipher text
decrypted = ""
for w in words:
    w = w.lower()
    if w == ' ': 
        decrypted += ' '
    elif 'a' <= w <= 'z':
        decrypted += chr(ord('a') + key.index(w))
    #else don't decode and print

# Test output
print words[:100]
print decrypted[:100]

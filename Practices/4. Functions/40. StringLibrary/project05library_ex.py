
# Symbolic constant

ASCII_LOWERCASE = 'abcdefghijklmnopqrstuvwxyz'


def capitalize( orig ):

    # Check first character of original string

    if orig[0] not in ASCII_LOWERCASE:

        # First character not lower case, so just copy original string
        
        new = orig[:]

    else:

        # First character lower case, so convert it to upper case
        # and copy remainder of original string.
        
        newchar = chr( ord( orig[0] ) - 32 )
        new = str( newchar ) + orig[1:]

    return new
